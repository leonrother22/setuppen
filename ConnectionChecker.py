import mysql.connector
from mysql.connector import Error
connection=None
cursor=None
Connected=False
try:
    connection = mysql.connector.connect(host='localhost',
                                     database='Lager',
                                     user='root',
                                     password='')
    cursor = connection.cursor(buffered=True)
    Connected=True
    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)

except Error as e:
    print("Error while connecting to MySQL", e)
